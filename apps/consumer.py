from flask import Flask, render_template, render_template_string

from connections import get_gpdb_connection, get_postgres_connection, get_mongo_database
from consumer_utils import get_page_template, get_pg_stats


app = Flask('Consumer')


@app.route('/')
def home():
    TMPL = get_page_template()
    # TODO Mongo stats
    pg_conn = get_postgres_connection()
    pg_rows = get_pg_stats(pg_conn)
    gpdb_conn = get_gpdb_connection()
    gp_rows = get_pg_stats(gpdb_conn)
    #return render_template('consumer_base.html', rows=rows)
    return render_template_string(TMPL, gp_rows=gp_rows, pg_rows=pg_rows)


app.run(host='0.0.0.0', port=8080, debug=True)