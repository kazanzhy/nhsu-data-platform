
def get_page_template():
    template = """
    <!DOCTYPE html>
    <html lang="en">
    <head><title>Greenplum Stats</title></head>
    <body>
        <h1>Postgres Stats</h1><hr>
        <ul id="navigation">{% for row in pg_rows %}
        <li>{{ row.schemaname }}.{{ row.tablename }} - <b>{{ row.numtuples }}</b> tuples</li>
        {% endfor %}</ul><hr>
        <h1>Greenplum Stats</h1><hr>
        <ul id="navigation">{% for row in gp_rows %}
        <li>{{ row.schemaname }}.{{ row.tablename }} - <b>{{ row.numtuples }}</b> tuples</li>
        {% endfor %}</ul><hr>
    </body>
    </html>
    """
    return template


def get_pg_stats(conn):
    query = """
        ANALYZE;
        SELECT schemaname, relname AS tablename, n_live_tup AS numtuples
        FROM pg_stat_all_tables 
        WHERE schemaname not in ('gp_toolkit', 'pg_catalog', 'pg_toast', 'information_schema')
        ORDER BY relname;
    """
    cursor = conn.execute(query)
    records = cursor.fetchall()
    cursor.close()
    return records
