## Monitoring
### MongoDB
```shell
docker exec -it dp_cdb_mongo mongo admin -u admin -p admin
rs.status()
use ehealth
db.getCollectionNames()
```

### PostgreSQL
```shell
docker exec -it dp_cdb_postgres psql ehealth admin 
\l
SELECT * FROM pg_stat_all_tables WHERE schemaname not in ('pg_catalog', 'pg_toast', 'information_schema');
```

### Greenplum
```shell
docker exec -it dp_greenplum_master psql -h localhost test gpadmin
\l
SELECT * FROM pg_stat_all_tables WHERE schemaname not in ('gp_toolkit', 'pg_catalog', 'pg_toast', 'information_schema');
```

### Broker
```shell
docker exec -it dp_kafka_broker bash;
kafka-topics --zookeeper kafka-zookeeper:2181 --list;
kafka-topics --zookeeper localhost:2181 --delete --topic mongo_ehealth.episode;
kafka-console-consumer --bootstrap-server kafka-broker:9092 --topic mongo_ehealth.episode --from-beginning;
kafka-console-consumer --bootstrap-server kafka-broker:9092 --topic postgres_ehealth.persons --from-beginning;
```

### Connect
```shell
docker exec -it dp_cdb_apps bash
curl http://kafka-connect:8083/connectors/ -X GET 
curl http://kafka-connect:8083/connectors/source-mongo -X DELETE
curl http://kafka-connect:8083/connectors/source-postgres -X DELETE
curl http://kafka-connect:8083/connectors/sink-greenplum-mongo -X DELETE
curl http://kafka-connect:8083/connectors/sink-greenplum-postgres -X DELETE
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/source-mongo.json
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/source-postgres.json
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/sink-greenplum-mongo.json
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/sink-greenplum-postgres.json
```

### kSQL
```shell
docker exec -it dp_kafka_ksqldb_cli ksql http://kafka-ksqldb-server:8088;
SHOW TOPICS;
CREATE STREAM pg_ehealth_persons WITH (KAFKA_TOPIC='postgres.ehealth.persons', VALUE_FORMAT='AVRO');
CREATE STREAM mg_ehealth_episode WITH (KAFKA_TOPIC='mongo.ehealth.episode', VALUE_FORMAT='AVRO');
SELECT * FROM pg_ehealth_persons EMIT CHANGES;
SELECT * FROM mg_ehealth_episode EMIT CHANGES;
```


## Problems
The main problem is to move data with complex structure from MongoDB to GreenplumDB (Postgres).
There are two ways to solve this problem:
1. **Avro Schema**
   1. Source: `Debezium CDC Connector -> SMT (ExtractNewDocumentState) -> AvroConverter (schema.enable=true)`;
   2. Sink: `AvroConverter (schema.enable=true) -> Record2JsonStringConverter -> JDBC Connector`;  
   
2. **Raw JSON**
   1. Source: `Debezium CDC Connector -> SMT (ExtractNewDocumentState) -> JsonConverter (schema.enable=true)`;
   2. ksqlDB processing:  
   ```sql
   CREATE STREAM raw_conditions (records VARCHAR) WITH (KAFKA_TOPIC='mongo.conditions', VALUE_FORMAT='KAFKA');
   SET 'auto.offset.reset' = 'earliest';
   CREATE STREAM wrapped_conditions WITH (FORMAT='AVRO') AS SELECT * FROM raw_conditions;
   ```
   3. Sink: `AvroConverter (schema.enable=true) -> Record2JsonStringConverter -> JDBC Connector`; 
- [Record2JsonStringConverter SMT repo](https://github.com/an0r0c/kafka-connect-transform-tojsonstring);  
- [Confluent Forum](https://forum.confluent.io/t/storing-topic-data-in-a-single-column-as-a-json-in-postgres/1207);


## Materials
* https://github.com/debezium/debezium-examples
* https://github.com/confluentinc/demo-scene/tree/master/kafka-connect-zero-to-hero
* https://debezium.io/documentation/reference/connectors/mongodb.html
* https://debezium.io/documentation/reference/connectors/postgresql.html
* https://debezium.io/documentation/reference/transformations/event-flattening.html
* https://debezium.io/documentation/reference/transformations/mongodb-event-flattening.html
* https://gpdb.docs.pivotal.io/6-0/greenplum-kafka/loading.html