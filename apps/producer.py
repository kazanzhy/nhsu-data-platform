import time
import random
import uuid
import datetime as dt

from connections import get_logger, get_mongo_database, get_postgres_connection
from producer_utils import patient_id_generator, string_generator, Persons, LegalEntities, Divisions, Employees

TABLES = [Persons, LegalEntities, Divisions, Employees]
COLLECTIONS = ['episode', 'encounter', 'condition', 'service_request', 'observation', 'diagnostic_report']


logger = get_logger()
mg_client, mg_db = get_mongo_database()
pg_conn = get_postgres_connection()

while True:
    for COLL in COLLECTIONS:
        data = {
            '_id': str(uuid.uuid4()),
            'patient_id': patient_id_generator(),
            'value': random.random(),
            'strings': [{'primary': string_generator()}, {'secondary': string_generator()}],
            'inserted_at': dt.datetime.utcnow()
        }
        try:
            mg_db[COLL].insert_one(data)
        except BaseException as err:
            logger.error(f"Not inserted into collection '{COLL}' ({str(err)})")
        else:
            logger.info(f"Inserted into collection '{COLL}' ({data})")
        time.sleep(2)

    for Table in TABLES:
        try:
            ins = Table.insert().values()
            result = pg_conn.execute(ins)
        except BaseException as err:
            logger.error(f"Not inserted into table '{Table}' ({str(err)})")
        else:
            logger.info(f"Inserted into table '{Table}': {result.inserted_primary_key}")
        time.sleep(2)
else:
    logger.info("Ending apps")
