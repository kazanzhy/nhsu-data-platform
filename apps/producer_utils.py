import string
import uuid
from datetime import datetime, timedelta
from hashlib import sha256
from random import randint, choices, choice

from sqlalchemy.sql.schema import MetaData, Table, Column, ForeignKey
from sqlalchemy.sql.sqltypes import Boolean, Integer, Float, String, Text, Date, DateTime
from sqlalchemy.dialects.postgresql import UUID, ARRAY, TIMESTAMP, JSON, TEXT


def patient_id_generator(patient: int = None, min_id: int = 0, max_id: int = 10) -> str:
    if not patient:
        patient = randint(min_id, max_id)
    patient_bin = patient.to_bytes(256, byteorder='big')
    hash_bin = sha256(patient_bin)
    hash_hex = hash_bin.hexdigest()
    return hash_hex.upper()

def date_generator():
    from_century = 365 * randint(0,121) + randint(0,365)
    dat = datetime(1900, 1, 1) + timedelta(days=from_century)
    return dat.date()

def string_generator(min_len: int = 2, max_len: int = 10) -> str:
    length = randint(min_len, max_len)
    text = ''.join(choices(string.ascii_lowercase, k=length))
    return text.title()

def gender_generator():
    return choice(['Male', 'Female', 'X-gender'])

def int_array_generator(min_len: int = 1, max_len: int = 10):
    length = randint(min_len, max_len)
    arr = [randint(0,100) for _ in range(length)]
    return arr

# Create bases for declaring tables
DBmetadata = MetaData(schema='ehealth')

Declarations = Table(
    'declarations', DBmetadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column('patient_id', String, nullable=True, default=patient_id_generator),
    Column('birth_date', Date, nullable=True, default=date_generator),
    Column('death_date', Date, nullable=True),
    Column('gender', String, nullable=True, default=gender_generator),
    Column('status', String(18), default='NEW'),
    Column('created_at', TIMESTAMP, default=datetime.utcnow, nullable=False),
    Column('updated_at', TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False),
)

Persons = Table(
    'persons', DBmetadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column('patient_id', String, nullable=True, default=patient_id_generator),
    Column('legal_entity_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('division_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('employee_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('status', String(18), default='NEW'),
    Column('created_at', TIMESTAMP, default=datetime.utcnow, nullable=False),
    Column('updated_at', TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False),
)

LegalEntities = Table(
    'legal_entities', DBmetadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column('name', String, nullable=True, default=string_generator),
    Column('divisions', ARRAY(Integer), nullable=True, default=int_array_generator),
    Column('email', String, nullable=True),
    Column('created_at', TIMESTAMP, default=datetime.utcnow, nullable=False),
    Column('updated_at', TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False),
)

Divisions = Table(
    'divisions', DBmetadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column('name', String, nullable=True, default=string_generator),
    Column('legal_entity_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('created_at', TIMESTAMP, default=datetime.utcnow, nullable=False),
    Column('updated_at', TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False),
)

Employees = Table(
    'employees', DBmetadata,
    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column('first_name', String, nullable=True, default=string_generator),
    Column('patronymic_name', String, nullable=True, default=string_generator),
    Column('last_name', String, nullable=True, default=string_generator),
    Column('legal_entity_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('division_id', UUID(as_uuid=True), default=uuid.uuid4),
    Column('created_at', TIMESTAMP, default=datetime.utcnow, nullable=False),
    Column('updated_at', TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False),
)

if __name__ == '__main__':
    DBmetadata.create_all()