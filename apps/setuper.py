import os
import time
import json
from pathlib import Path
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError

from sqlalchemy.schema import CreateSchema

from connections import get_logger, get_mongo_database, get_postgres_connection, get_gpdb_connection
from producer_utils import DBmetadata


def request(link: str, body: str = '{}'):
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    req = Request(link, body.encode(), headers, method='POST')
    try:
        with urlopen(req) as resp:
            response = json.load(resp)
    except HTTPError as err:
        return int(err.code), err.reason
    except URLError as err:
        return 1, err.reason
    except BaseException as err:
        return 0, str(err)
    else:
        return resp.status, response


def init_mongo_replica_set() -> None:
    logger = get_logger()
    client, _ = get_mongo_database()
    try:
        admin_db = client['admin']
        settings = {} # {'_id': "rs", 'members': [{'_id': 0, 'host': "cdb-mongo:27017"}]}
        admin_db.command('replSetInitiate', settings)
    except BaseException as err:
        logger.error(f'replSetInitiate - ERROR ({str(err)})')


def init_postgres():
    logger = get_logger()
    conn = get_postgres_connection()
    try:
        if not conn.dialect.has_schema(conn, 'ehealth'):
            conn.execute(CreateSchema('ehealth'))
    except BaseException as err:
        logger.error(f'Could not create schema "ehealth" in Postgres: {str(err)}')
    try:
        DBmetadata.drop_all(conn)
        DBmetadata.create_all(conn)
    except BaseException as err:
        logger.error(f'Could drop or create tables in Postgres: {str(err)}')


def init_greenplum():
    logger = get_logger()
    conn = get_gpdb_connection()
    schemas = ['raw_postgres_ehealth', 'raw_mongo_ehealth']
    for schemaname in schemas:
        try:
            if not conn.dialect.has_schema(conn, schemaname):
                conn.execute(CreateSchema(schemaname))
        except BaseException as err:
            logger.error(f'Could not create schema {schemaname} in Greenplum: {str(err)}')


def add_kafka_connect_configs():
    connect_host = os.environ.get('CONNECT_REST_ADVERTISED_HOST_NAME', 'kafka-connect')
    connect_port = os.environ.get('CONNECT_REST_PORT', '8083')
    files = list(Path('connect').glob('*.json'))
    while files:
        connect = files.pop(0)
        url = f'http://{connect_host}:{connect_port}/connectors'
        status, resp = request(url, connect.read_text())
        print(f'Connect: url={url}, file={connect.name}, status={status}, response={resp}')
        if status in [0,1]:
            files.append(connect)
        time.sleep(3)


if __name__ == '__main__':
    init_mongo_replica_set()
    init_postgres()
    init_greenplum()
    add_kafka_connect_configs()