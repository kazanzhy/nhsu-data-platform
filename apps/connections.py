import os
import logging

from sqlalchemy.engine import create_engine, Engine
from pymongo import MongoClient


def get_mongo_database():
    host = os.environ.get('CDB_MG_HOST')
    port = os.environ.get('CDB_MG_PORT')
    db = os.environ.get('CDB_MG_DB')
    user = os.environ.get('CDB_MG_USER')
    pwd = os.environ.get('CDB_MG_PASS')
    conn_uri = f'mongodb://{user}:{pwd}@{host}:{port}/?authSource=admin'
    client = MongoClient(conn_uri)
    database = client.get_database(db)
    return client, database


def get_postgres_connection() -> Engine:
    host = os.environ.get('CDB_PG_HOST')
    port = os.environ.get('CDB_PG_PORT')
    db = os.environ.get('CDB_PG_DB')
    user = os.environ.get('CDB_PG_USER')
    pwd = os.environ.get('CDB_PG_PASS')
    conn_uri = f'postgresql+psycopg2://{user}:{pwd}@{host}:{port}/{db}'
    engine = create_engine(conn_uri)
    return engine.connect()

def get_gpdb_connection():
    host = os.environ.get('GPDB_HOST')
    port = os.environ.get('GPDB_PORT')
    db = os.environ.get('GPDB_DB')
    user = os.environ.get('GPDB_USER')
    pwd = os.environ.get('GPDB_PASS')
    conn_uri = f'postgresql+psycopg2://{user}:{pwd}@{host}:{port}/{db}'
    engine = create_engine(conn_uri)
    return engine.connect()

def get_logger() -> logging.Logger:
    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
    logger = logging.getLogger('Apps')
    return logger