# NHSU data platform

Proof of concept project. 
Example of data replication from MongoDB to PostgreSQL using Apache Kafka.
## High level
```mermaid
graph LR
    subgraph "Ehealth"
    cdb[Central Databases]
    end

    subgraph "Other services"
    registries[Registries]
    apis[APIs]
    end

    subgraph "NHSU"
    streaming[Streaming]
    mpp[MPP Data warehouse]
    orchestration[Orchestration]
    monitoring[Monitoring apps]
    analysts[Analysts]
    bi[BI apps]
    end

    registries --> streaming
    apis --> streaming
    cdb --> streaming
    streaming --> mpp
    mpp --> analysts
    mpp --> bi
    mpp --> monitoring
    streaming --> monitoring
```

## Low level
After the `docker-compose up` starts all instances described in graph below.
```mermaid
graph LR
    subgraph "External services"
    registries[Registries]
    apis[APIs]
    cdb-postgres[Central DB: PostgreSQL]
    cdb-mongo[Central DB: MongoDB]
    end

    registries --> kafka-connect-src
    apis --> kafka-connect-src
    cdb-postgres --> kafka-connect-src
    cdb-mongo --> kafka-connect-src

    subgraph "Streaming"
    kafka-connect-src[Kafka Connect: source]
    kafka-connect-snk[Kafka Connect: sink]
    kafka-broker[Kafka Brokers]
    ksqlDB[Confluent ksqlDB]
    schema-registry[Confluent Schema Registry]
    kafka-connect-src --> kafka-broker
    kafka-broker --> kafka-connect-snk
    kafka-broker --> ksqlDB
    end

    subgraph "Warehouse"
    dbt[data build tool]
    greenplum-master[GreenplumDB Master]
    end

    kafka-connect-snk --> greenplum-master
    greenplum-master --> power-bi

    subgraph "Analytics"
    power-bi[PowerBI]
    analyst[Analysts]
    monitoring[Monitoring]
    end

    greenplum-master ---> monitoring
    greenplum-master ---> analyst
    ksqlDB --> monitoring
```
Visit http://localhost:8080 for some stats.  
GreenplumDB - postgres://localhost:54321.  
PostgreSQL - postgres://localhost:5432.  
MongoDB - mongo://localhost:27017.  