#!/usr/bin/env sh
set -m;
cd $HOME || exit;
sleep 35s;

# Run Kafka Connect settings in background
python3 setuper.py &
SETUPER_PID=$!;

# Run Kafka Connect in background
python3 producer.py &
PRODUCER_PID=$!;

# Run Kafka Connect in background
python3 consumer.py &
CONSUMER_PID=$!;

# Continuous monitoring producer and consumer
while sleep 60; do
  if [ -d /proc/${SETUPER_PID} ] || [ -d /proc/${PRODUCER_PID} ] || [ -d /proc/${CONSUMER_PID} ]; then
    echo "$(date) - Python apps check:";
    [ -d /proc/${SETUPER_PID} ] && echo "Setuper ($SETUPER_PID) - WORKING" || echo "Setuper - DONE";
    [ -d /proc/${PRODUCER_PID} ] && echo "Producer ($PRODUCER_PID) - WORKING" || echo "Producer - DOWN";
    [ -d /proc/${CONSUMER_PID} ] && echo "Consumer ($CONSUMER_PID) - WORKING" || echo "Consumer - DOWN";
  else
    exit 1;
  fi;
done;